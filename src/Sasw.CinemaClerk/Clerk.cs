using System.Collections.Generic;

namespace Tickets
{
    public class Clerk
    {
        private readonly ChangeManager _changeManager;

        public Clerk(int ticketPrice)
        {
            _changeManager = new ChangeManager(ticketPrice);
        }
        
        public bool IsPossible(int [] payments)
        {
            foreach (var payment in payments)
            {
                var isTicketSold = _changeManager.IsChangeAvailable(payment);
                if (!isTicketSold)
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}