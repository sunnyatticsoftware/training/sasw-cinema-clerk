﻿using System;

namespace Tickets
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Running Cinema Clerk..");
            
            // clerk.IsPossible(new int[] {25, 25, 50})           // => YES
            // clerk.IsPossible(new int[] {25, 100})              // => NO 
            // clerk.IsPossible(new int[] {25, 25, 50, 50, 100})  // => NO
            
            const int ticketPrice = 25;
            var clerk = new Clerk(ticketPrice);
            var queue = new[] {25, 25, 50, 50, 100};
            var result = clerk.IsPossible(queue);
            Console.WriteLine($"RESULT: {result}");
            
            Console.WriteLine("Press [ENTER] to end program");
            Console.ReadLine();
        }
    }
}