using System;
using System.Collections.Generic;

namespace Tickets
{
    public class ChangeManager
    {
        private readonly int _ticketPrice;

        private const int Note25 = 25;
        private const int Note50 = 50;
        private const int Note100 = 100;

        private readonly IDictionary<int, int> _notes =
            new Dictionary<int, int>
            {
                {Note25, 0},
                {Note50, 0},
                {Note100, 0}
            };

        public ChangeManager(int ticketPrice)
        {
            _ticketPrice = ticketPrice;
        }

        public bool IsChangeAvailable(int noteValue)
        {
            var change = noteValue - _ticketPrice;
            var isInvalid = change < 0;
            if (isInvalid)
            {
                return false;
            }

            if (change == 0)
            {
                AddNote(noteValue);
                return true;
            }

            try
            {
                var hundreds = change / Note100;
                RemoveNote(Note100, hundreds);
                var fifties = (change % Note100) / Note50;
                RemoveNote(Note50, fifties);
                var twentyFives = ((change % Note100) % Note50) / Note25;
                RemoveNote(Note25, twentyFives);

                AddNote(noteValue);

                return true;
            }
            catch (ApplicationException)
            {
                return false;
            }
        }

        private void AddNote(int noteValue)
        {
            if (!_notes.ContainsKey(noteValue))
            {
                throw new Exception($"Note {noteValue} not supported");
            }
            _notes[noteValue]++;
        }
        
        private void RemoveNote(int noteValue, int count)
        {
            if (!_notes.ContainsKey(noteValue))
            {
                throw new Exception($"Note {noteValue} not supported");
            }

            var newNoteCount = _notes[noteValue] - count;
            if (newNoteCount < 0)
            {
                throw new ApplicationException($"Not enough {noteValue} notes");
            }

            _notes[noteValue] = newNoteCount;
        }
    }
}