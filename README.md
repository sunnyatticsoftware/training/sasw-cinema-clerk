# sasw-cinema-clerk

Algorithm to determine whether cinema tickets can be sold to people queuing without any available change

## Instructions

There are a lot of people at the cinema box office standing in a huge line.
Each of them has a single 100, 50 or 25 dollar bill. Movie ticket costs 25 dollars.

Clerk wants to sell a ticket to every single person in this line.

Can clerk sell a ticket to every person and give change if he
initially has no money and sells the tickets strictly in the order people queue?

Return YES, if clerk can sell a ticket to every person and give change with the bills he has at hand at that moment.
Otherwise return NO.

Examples:
- Line.Tickets(new int[] {25, 25, 50})           // => YES
- Line.Tickets(new int[] {25, 100})              // => NO 
- Line.Tickets(new int[] {25, 25, 50, 50, 100})  // => NO
